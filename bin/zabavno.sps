#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; PC emulator in Scheme
;; Copyright © 2016, 2019 Göran Weinholt <goran@weinholt.se>

;; Permission is hereby granted, free of charge, to any person obtaining a
;; copy of this software and associated documentation files (the "Software"),
;; to deal in the Software without restriction, including without limitation
;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;; and/or sell copies of the Software, and to permit persons to whom the
;; Software is furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included in
;; all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;; DEALINGS IN THE SOFTWARE.
#!r6rs

(import (rnrs (6))
        (zabavno cpu x86)
        (prefix (zabavno hardware ich8) ich8:)
        (zabavno firmware pcbios)
        (zabavno loader pcboot))

(define (help)
  (display "The zabavno machine emulator\n\
Usage: zabavno [--debug] [--trace] [-fda floppy.img] [-hda disk.img]\n\
\n"
           (current-error-port)))

(define (open-file-image-port filename)
  (guard (exn
          ((i/o-file-protection-error? exn)
           ;; Try it read-only.
           (open-file-input-port filename)))
    (open-file-input/output-port filename
                                 (file-options no-create no-fail no-truncate))))

(define (parse-command-line! M bios-data command-line)
  (let lp ((arg* (cdr command-line)))
    (unless (null? arg*)
      (let ((arg (car arg*)))
        (cond
          ((string=? arg "--help")
           (help)
           (exit 0))
          ((string=? arg "--debug")
           (machine-debug-set! M #t)
           (lp (cdr arg*)))
          ((string=? arg "--trace")
           (machine-trace-set! M #t)
           (lp (cdr arg*)))
          ((string=? arg "-fda")
           (when (null? (cdr arg*)) (help) (exit 1))
           (let ((p (open-file-image-port (cadr arg*))))
             (load-boot-sector M p 'floppy)
             (pcbios-load-floppy-image M bios-data 0 p)
             (lp (cddr arg*))))
          ((string=? arg "-fdb")
           (when (null? (cdr arg*)) (help) (exit 1))
           (let ((p (open-file-image-port (cadr arg*))))
             (pcbios-load-floppy-image M bios-data 1 p)
             (lp (cddr arg*))))
          ((string=? arg "-hda")
           (when (null? (cdr arg*)) (help) (exit 1))
           (let ((p (open-file-image-port (cadr arg*))))
             (load-boot-sector M p #f)
             (pcbios-load-harddrive-image M bios-data 0 p)
             (lp (cddr arg*))))
          (else
           (help)
           (exit 1)))))))

(define (main command-line)
  (let ((M (make-machine)))
    (ich8:init M)
    (enable-interrupt-hooks M)
    (let ((bios-data (pcbios-setup M)))
      ;; Handle command line arguments.
      (parse-command-line! M bios-data command-line)
      ;; Run.
      (let lp ()
        (case (machine-run M)
          ((stop) #f)
          ((hlt) (lp))          ;wait for external interrupts
          ((reboot)
           (main command-line))
          (else (lp)))))))

(main (command-line))
