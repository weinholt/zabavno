#!/bin/sh
set -ex

cleanup () {
  rm -f generate.out hello.com
}
trap cleanup EXIT

# Check that the programs can be started at all.
bin/zabavno.sps --help
bin/zabavno-dos.sps

# Run a simple DOS program.
tests/hello-dos.sps
bin/zabavno-dos.sps hello.com

# The instruction tester.
case $(uname -m) in
    x86_64|i386)
        if command -v scheme >/dev/null; then
                # Fails on AMD CPUs due to differences in SHDL/SHRD.
                if grep -q '^model name.*Intel' /proc/cpuinfo; then
                    tests/x86/generate.sps && chmod +x generate.out
                    ./generate.out
                fi
        fi
esac
